@extends('layouts.dashboard')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Практика</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <p class="lead text-center">Practice with Zack</p>
                    <hr>
                    <span class="text-muted">
                        Сайт рыбатекст поможет дизайнеру, верстальщику,
                        вебмастеру сгенерировать несколько абзацев более менее осмысленного
                        текста рыбы на русском языке, а начинающему оратору отточить навык
                        публичных выступлений в домашних условиях. При создании генератора мы использовали
                        небезизвестный универсальный код речей.
                    </span>
                </div>
            </div>
        </div>
    </div>
@endsection

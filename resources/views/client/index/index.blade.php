@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron" style="background-color: lightslategreye;">
                    <div class="row">
                        <div class="col-md-9">
                            <h1 class="display-4">Master Your English</h1>
                            <p class="lead">
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать
                                несколько абзацев более менее осмысленного текста рыбы на русском языке,
                                а начинающему оратору отточить навык публичных выступлений в домашних условиях.
                            </p>
                        </div>
                        <div class="col-md-1">
                            <img src="img/student.png" class="img-fluid" style="max-width: 200px;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <blockquote class="blockquote text-center">
                    <p class="lead">Пока все хотят купить Rolls Royce я хочу купить компанию Rolls Royce.</p>
                    <footer class="blockquote-footer"><cite title="Source Title">Zack Shatrov</cite></footer>
                </blockquote>
            </div>
            <div class="col-md-4">
                <div class="card shadow-border mb-3">
                    <div class="card-body text-dark">
                        <h5 class="card-title"><a href="#">Простое обучение</a></h5>
                        <p class="card-text">
                            Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать
                            несколько абзацев более менее осмысленного текста рыбы на русском языке,
                            а начинающему оратору отточить навык публичных выступлений в домашних условиях.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card shadow-border mb-3">
                    <div class="card-body text-dark">
                        <h5 class="card-title"><a href="#">Практика online</a></h5>
                        <p class="card-text">
                            Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать
                            несколько абзацев более менее осмысленного текста рыбы на русском языке,
                            а начинающему оратору отточить навык публичных выступлений в домашних условиях.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card shadow-border mb-3">
                    <div class="card-body text-dark">
                        <h5 class="card-title"><a href="#">Доброжелательное комьюнити</a></h5>
                        <p class="card-text">
                            Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать
                            несколько абзацев более менее осмысленного текста рыбы на русском языке,
                            а начинающему оратору отточить навык публичных выступлений в домашних условиях.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection